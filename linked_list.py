from classes import Node
from classes import Error

items = dict()

# items['name']


class Item:
    def __init__(self,initdata):
        self.data = initdata
        self.next = None
        self.prev = None

    def getData(self):
        return self.data

    def getNext(self):
        return self.next

    def getPrev(self):
        return self.prev

    def setData(self,newdata):
        self.data = newdata

    def setNext(self,newnext):
        self.next = newnext

    def setPrev(self,newPrev):
        self.prev = newPrev

class UnorderedList:
    def __init__(self):
        self.head = None
        self.tail = None

    def isEmpty(self):
        return self.head == None

    def add(self,item, index = None):

        temp = Item(item)

        if index == None or index >=self.size()-1:
            if self.isEmpty():
                self.head = self.tail = temp
            else:
                self.tail.setNext(temp)
                temp.setPrev(self.tail)
                self.tail = temp
        elif index<=0:
            # move the head
            temp.setNext(self.head)
            self.head.setPrev(temp)
            self.head = temp

        else:
            former = self.goToIdx(index)
            temp.setNext(former)
            temp.setPrev(former.getPrev())
            # print()
            temp.getPrev().setNext(temp)
            former.setPrev(temp)

            # move the tail

        # print('===new===')
        # print('head: ')
        # print(self.head)
        # print('tail:')
        # print(self.tail)
        # print(temp)
        # print(temp.getNext())
        # print(temp.getPrev())
        # print('=========')
        # self.printAll()

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count + 1
            current = current.getNext()

        return count

    def search(self,value):
        current = self.head
        found = False
        while current != None and not found:
            if current.getData() == value:
                # found = True
                return current
            else:
                current = current.getNext()

        return None

    def printAll (self):
        current = self.head
        while current is not None:
            # print(current)
            print (current.getData())
            # print('next: ')
            # print(current.getNext())
            # print('prev:' )
            # print(current.getPrev())
            current = current.getNext()


    def remove(self,item):
        if item == None:
            return None
        if item == self.head:
            self.head = item.getNext()
            self.head.setPrev(None)
        elif item == self.tail:
            self.tail = item.getPrev()
            self.tail.setNext(None)
        else:
            # print(item)
            item.getPrev().setNext(item.getNext())
            item.getNext().setPrev(item.getPrev())
            item.setNext(None)
            item.setPrev(None)

    #     if item == None:
    #         return None
    #     current = self.head
    #     previous = None
    #     found = False
    #     while current!= None:
    #         # print(item)
    #         print(current)
    #         if current.getData() == item.getData():
    #             # print('found sth')
    #             found = True
    #         else:
    #             previous = current
    #             current = current.getNext()
    #
    #     if not found:
    #         return None
    #
    #     # remove operation
    #     if previous == None:
    #         self.head = current.getNext()
    #     else:
    #         previous.setNext(current.getNext())
    #
    #
    def update(self,item, newData):
        if item == None:
            return None
        item.setData(newData)
    #
    #     current = self.head
    #     previous = None
    #     found = False
    #     while current!= None:
    #         print('----- -')
    #         print(item)
    #         print(current)
    #         print('------')
    #
    #         if current.getData() == item.getData():
    #             # print('found sth')
    #             found = True
    #         else:
    #             previous = current
    #             current = current.getNext()
    #
    #     if not found:
    #         return None
    #     else:
    #         current.setData(newdata)
    #         return current
    #     # remove operation
    #     # if previous == None:
    #         # self.head = current.getNext()
    #     # else:
    #         # previous.setNext(current.getNext())
    #
    #
    def goToIdx(self,idx):
        current = self.head
        for i in range (0,idx):
            if current == None:
                current = None
                # return None
                break
            current = current.getNext()

        return current

    # def update (self, oldItem, newItem):
    #     current = self.head
    #     previous = None
    #     found = False
    #     while not found:
    #         if current.getData() == item.getData():
    #             # print('found sth')
    #             found = True
    #         else:
    #             previous = current
    #             current = current.getNext()
    #
    #     if previous == None:
    #         self.head = current.getNext()
    #     else:
    #         previous.setNext(current.getNext())

def create(name:str):
    items[name] = UnorderedList()

def add(name:str, newElem, index=None):
        items[name].add(newElem, index)
    # else add ke sebuah index



def update(name:str, newElem, index=None):
    if index == None:
        items[name].update(items[name].tail,newElem)

    else:
        # print(index)
        item = items[name].goToIdx(index)
        # print(item)
        items[name].update(item, newElem)

def remove (name:str, index=None):
    if index == None:
        # items[name]=UnorderedList()
        items[name].remove(items[name].goToIdx(items[name].size()-1))

    else:
        # print(index)
        item = items[name].goToIdx(index)
        # print(items[name].goToIdx(index))
        # print(item)
        # items[name].remove(item)
        items[name].remove(item)


def printLL(name:str, index=None):
    if index == None:
        items[name].printAll()
    else:
        print(items[name].goToIdx(index).getData())


# # test
# create('newLL')
# add('newLL', 1)
# add('newLL', 2)
# add('newLL', 3)
# update ('newLL',8, 1)
# remove('newLL',1)
# printLL('newLL')
#
# create('anothr LL')
# add('anothr LL', 5)
# add('anothr LL', 6)
# add('anothr LL', 7)
# remove('anothr LL',0)
# printLL('anothr LL')


create('nama')
add('nama', 10)
add('nama', 11)
add('nama', 13)
add('nama', 15)


class Command:
    def __init__(self, command:str):
        self.command = command

def exec_s(node,arguments):
    # print('exec_s==========')
    # print('get from leaf: ')
    # print(arguments)

    command = arguments[0]
    if command =="add":
        name = arguments[1]
        if node.children[0].symbol=='last':
            # print('printing to the last')
            add(name,arguments[2])
            # printLL(name)
        else:
            # check wether the index is valid
            try:
                index = int(node.children[0].token.value)
                if index>=items[name].size():
                    error = Error('INDEX_OUT_OF_BOUND', index)
                    # print(error)
                    # print('outofbound')
                    print(error.message)
                    # exit()
                    return error
                else:
                    # print('processing')
                    add(name, arguments[2], index)
                    # printLL('nama')
            except ValueError:
                # print('error')
                error = Error('INVALID_INDEX_FORMAT', node.children[0].symbol)
                print(error.message)
                return error
        # printLL('nama')
    elif command == "update":
        name = arguments[1]
        if node.children[0].symbol=='last':
            if items[name].isEmpty():
                error = Error('NO_LL')
                print(error.message)

                return error
            else:
                update(name,arguments[2])
                # add(name,arguments[2])
                # printLL(name)
        else:
            # check wether the index is valid
            try:
                index = int(node.children[0].token.value)
                if index>=items[name].size():
                    error = Error('INDEX_OUT_OF_BOUND', index)
                    print(error.message)

                    return error
                else:
                    # print('processing')
                    update(name, arguments[2], index)
                    # printLL('nama')
            except ValueError:
                # print('error')
                error = Error('INVALID_INDEX_FORMAT', node.children[0].symbol)
                print(error.message)
                return error
        # printLL('nama')
    elif command=="create":
        name = node.children[0].token.value
        create(name)


    if command == "clear":
        name = arguments[1]
        if node.children[0].symbol=='last':
            if items[name].isEmpty():
                error = Error('NO_LL')
                print(error.message)
                return error
            else:
                remove(name)
                # add(name,arguments[2])
                # printLL(name)
        else:
            # check wether the index is valid
            try:
                index = int(node.children[0].token.value)
                if index>=items[name].size():
                    error = Error('INDEX_OUT_OF_BOUND', index)
                    print(error.message)
                    return error
                else:
                    # print('processing')
                    remove(name, index)
                    # printLL(name)
            except ValueError:
                # print('error')
                error = Error('INVALID_INDEX_FORMAT', node.children[0].symbol)
                print (error)
                return error
        # printLL('nama')
    #
    # if command.command == "print":
    #     print(arguments[0], arguments[1])

    if command == "print":
        name = arguments[1]
        if node.children[0].symbol=='last':
            if items[name].isEmpty():
                error = Error('NO_LL')
                print(error.message)
                return error
            else:
                printLL(name)
                # add(name,arguments[2])
        else:
            # check wether the index is valid
            try:
                index = int(node.children[0].token.value)
                if index>=items[name].size():
                    error = Error('INDEX_OUT_OF_BOUND', index)
                    print(error.message)
                    return error
                else:
                    # print('processing')
                    printLL(name, index)
            except ValueError:
                # print('error')
                error = Error('INVALID_INDEX_FORMAT', node.children[0].symbol)
                print(error.message)
                return error
    # print(arguments)

def exec_b (node,arguments):

    # arguments = dict()
    # arguments['exec_b']='OK'
    # arguments = list()

    # check for value validity
    # print('exec_b==========')
    # print('get from leaf: ')
    # print(arguments)
    name = arguments[1]
    try:
        number = int(node.children[0].token.value)
        arguments.append(number)
        return arguments
    except ValueError:
        error = Error('INVALID_NUMBER',number)
        print(error.message)
        return error

    # arguments.append('OK')

    return arguments

def exec_b_ (node,arguments):
    # print('exec_b_==========')
    # print('get from leaf: ')
    # print(arguments)

    name = node.children[0].token.value
    # print(name)
    # check availability of name:
    if name in items:
        arguments.append(name)
        return arguments
    else:
        error = Error ('NO_LL', name)
        print(error.message)
        return error



def exec_d (node,arguments):
    # arguments = dict()
    # arguments['exec_d']='OK'
    # arguments = list()
    # arguments.append("OK")
    # return arguments

    name = node.children[0].token.value
    if name in items:
        arguments.append(name)
        return arguments
    else:
        error = Error ('NO_LL', name)
        print(error.message)
        return error

#!!! warning, one grammar shall not two or more the same terminal
def getValue (node,arguments):
    # print('get value')
    # arguments = dict()
    # arguments[node.symbol]='OK'
    # arguments = list()
    for child in node.children:
        arguments.append(child.symbol)
    # arguments.append(node.children)
    return arguments


# run
