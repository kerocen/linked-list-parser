from classes import Grammar
from classes import Terminal
from classes import Token


class Lexer:
    def __init__(self, grammarList, terminalList):
        self.gramarList = grammarList
        self.terminalList = terminalList
        self.symbolTable = dict()
        self.__initSymbolTable(self.terminalList)
        # init symbolTable from the grammarList
        symbolTable = dict()

    def printGrammar(self):
        try:
            if self.gramarList:
                raise ValueError

            for grammar in grammarList:
                grammar.print()
        except ValueError:
            print("grammarList is empty")

    def __initSymbolTable(self, terminalList):
        for key, value in self.terminalList.items():
            symbolList = value.values
            for symbol in symbolList:
                self.symbolTable[symbol] = value

    def __populateSymbolTable(self,token):
        # if a token is registered in symboltable, return the representation
        if token in self.symbolTable:
            return Token(self.symbolTable[token].name,token)
        # else:
            # self.symbolTable[token] = self.terminalList['id']
            # return self.symbolTable[token]

        # else, populate the symboltable
        else:
            try:
                token = int(token)
                self.symbolTable[token] = self.terminalList['num']
            except ValueError:
                self.symbolTable[token] = self.terminalList['name']
            finally:
                return Token(self.symbolTable[token].name, token)

    def printSymbolTable(self):
        # print(self.symbolTable)
        for key,value in self.symbolTable.items():
            print(key, end='=>')
            print(value.name)

    def tokenize(self,inputStr:str):
        lexemes = []
        tokens = inputStr.split()
        for token in tokens:
            lexemes.append(self.__populateSymbolTable(token))

        return lexemes


# init dummy grammarList
s1 = Grammar('S',['A','name'])
s2 = Grammar('S',['B','C'])
s3 = Grammar('S',['D','C'])

a = Grammar('A','a')

b = Grammar('B',['B_','number'])

b_ = Grammar('B_',['b','name'])
grammarList = [s1,s2,s3,a,b,b_]

# for grammar in grammarList:
    # grammar.print()

# list of built in terminal
terminalList = {
        'name': Terminal('name',[],False),
        'num': Terminal('num',[],False),
        'add': Terminal('add',['add'],False),
        'update': Terminal('update',['update'],False),
        'create': Terminal('create',['create'],False),
        'clear': Terminal('clear',['clear'],False),
        'print': Terminal('print',['print'],False),
        'at_idx': Terminal('at_idx',['at_idx'],False),
        'null': Terminal('null',['null'],False)
        # 'name': Terminal('name',nameRegex,True),
        # 'element': Terminal('name',nameRegex,True),
    }


# def printSymbolTable():
#     for key,value in symbolTable.items():
#         print(key, end='=>')
#         print(value.key)
#
# def populateSymbolTable(token):
#     # if a token is registered in symboltable, return the representation
#     if token in symbolTable:
#         return symbolTable[token]
#
#     # else, populate the symboltable
#     else:
#         try:
#             token = int(token)
#             symbolTable[token] = terminalList['num']
#         except ValueError:
#             symbolTable[token] = terminalList['name']
#         finally:
#             return symbolTable[token]



# return the reprensentation or None if not found
# lexemes = []

# def run():
#     userInput = input('>')
#     tokens = userInput.split()
#
#     for token in tokens:
#         lexemes.append(populateSymbolTable(token))
#         # print(token, end=': ')
#         # print(lexemes[len(lexemes)-1].key)
#
# def tokenize(inputStr:str):
#     tokens = inputStr.split()
#     for token in tokens:
#         lexemes.append(populateSymbolTable(token))
#
#     return lexemes


# initSymbolTable(terminalList)
# printSymbolTable()
# run()

# for lexeme in lexemes:

# printSymbolTable()
# print (lexeme.key)
