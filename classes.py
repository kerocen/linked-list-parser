# from lexical_analysis import Token
# from linked_list import Error

class Error:
    def __init__(self, type, argument):
        if type == 'NO_LL':
            self.message = 'no linkedlist with name \''+argument+'\' was found'
        if type == 'INVALID_NUMBER':
            self.message = argument+' is not a valid number'
        if type == 'INDEX_OUT_OF_BOUND':
            self.message = 'index '+str(argument)+' is not exist'
        if type == 'INVALID_INDEX_FORMAT':
            self.message = 'index '+str(argument)+' is not in a valid format'

class Token:
    def __init__ (self, name, value):
        self.name = name
        self.value = value

class Edge:
    def __init__(self, parent, child, symbol,height):
        self.parent = parent
        self.child = child
        self.symbol = symbol
        self.height = height

class Node:
    def __init__(self, token, translation,parent = None, children = None):
        self.parent = parent
        self.translation = translation
        if children == None:
            self.children = list()
        else:
            self.children = children

        self.token = token
        if isinstance(token,Token):
            self.symbol = token.name
        else:
            self.symbol = token

    def trans(self):
        # arguments = dict()
        arguments = list()
        for child in reversed(self.children):

            # arguments=dict(arguments.items()+ child.trans().items())
            # tmp = child.trans()
            # arguments={**arguments, **child.trans()}
            newArgs = child.trans()
            # print(newArgs)
            if isinstance(newArgs, Error):
                x=1
                # print('error')
                # print(newArgs.message)
                # exit()
                continue
            arguments+=newArgs


        # print("trans "+self.symbol)
        return self.translation(self,arguments)

# check if it is a non-terminal (a capital letter)
def isNonTerminal(char):
    return char.isupper()

# left is a string, right should be a list
class Terminal:
    def __init__(self,name,values, hasRegex):
        self.name = name
        self.values = values
        self.hasRegex = hasRegex

    def addValue(self, value):
        if hasRegex:
            print("error, adding value into a regex terminal")
            sys.exit()
        self.values.append(value)

    def print(self):
        if self.hasRegex:
            print (value)
        else:
            print(self.name, end=" : ")
            for value in self.values:
                print(value, end=' ,')
            print('')


class Grammar:
    def __init__(self,left:str,right:str):
            self.left = left
            self.right = right

    def addRight(self,aList):
        self.right.append(aList)

    def getRightIndex(index:int):
        return self.right(index)
    # def print(self):
    #     nRight = len(self.right)
    #     print (self.left, end=' -> ')
    #     for i in range(0,nRight):
    #         for j in range (0,len(self.right[i])):
    #             atom = self.right[i][j]
    #             if isinstance(atom,Terminal):
    #                 print(atom.key,end='')
    #             else:
    #                 print (' '+self.right[i][j]+' ',end='')
    #         if i <nRight-1:
    #             print ('|', end='')

    def toString(self):
        string = ''
        string +=self.left+'->'
        for item in self.right:
            string+=item+' '
        return string

    def print(self):
        print(self.toString())
        # print('')

# class action:
#     def __init__(self, command, target):
#         self.command = command
#         self.target = target
#
#     def execute(self):
#         # SHIFT
#

# class parsingRow:
#     def __init__ (self, state, next, action):
# #         self.
#

# contains a grammar and its next item in dot using 0-based index
 # e.g. : Item ('Grammar ('S','A'), '0'), in hand written notation: S-> . A
class Item:
    def __init__(self,grammar:Grammar, grammarId:int ,next:int=0):
        self.grammar = grammar
        self.next = next
        self.grammarId = grammarId

    def getNext (self):
        return self.grammar.right[self.next]

    def isComplete(self):
        return self.next >= len(self.grammar.right)

    def toString(self):
        string = ''
        string +=self.grammar.left+'->'
        for i in range (len(self.grammar.right)):
            if (i == self.next):
                string += '. '
            string+=self.grammar.right[i]+' '
        return string

    def moveDot(self):
        self.next+=1

    def compare(self, anotherItem):
        return self.toString() == anotherItem.toString()

# the parsing table object
class ParsingTable:
    def __init__(self,grammarList, translationList, terminalTranslation, eofTerminal, acceptedTerminal):

        self.eofTerminal = eofTerminal
        self.acceptedTerminal = acceptedTerminal
        self.itemSetList = list()
        self.syntaxTree = None
        self.translationList = translationList
        self.terminalTranslation = terminalTranslation
        # Augment the grammar
        self.startingSymbol = grammarList[0].left+'_'
        augmentedGrammar = Grammar(self.startingSymbol,grammarList[0].left)
        itemSet0 = ItemSet(Item(augmentedGrammar,-1,0),0)
        for idx, grammar in enumerate(grammarList):
            itemSet0.addItem(Item(grammar,idx,0))

        self.itemSetList.append(itemSet0)
        self.grammarList = grammarList
        self.finalItemState = None
        self.followDict = dict()
        self.__calculateFollow()
        # self.printFollowDict()
        self.__closure()
        # self.generateParsingTable()

    def print(self):
        for i in range (0, len(self.itemSetList)):
            self.itemSetList[i].print()

    # do closure operation
    # moving the dot and create new ItemSet into the itemSetList
    def __closure(self, debug=False):
        acceptedFound = False
        finalState = None
        # for each item set
        currentitemSetId=0
        for itemSet in self.itemSetList:
            # for each item in the item set
            if debug:
                print("current item set # %d"%(currentitemSetId))
            for item in itemSet.items:
                # if the dot is in the end (next >=len)
                if debug:
                    print('current item: '+item.toString())
                if item.isComplete():
                    if debug:
                        print(' ^ is complete')
                    # if !acceptedFound  and that item set is the starting character
                    if finalState == None and item.grammar.left == self.startingSymbol:
                        # note the reference tho this item set to be used later in the parsing table
                        if debug:
                            print(" ^ final state")

                        itemSet.next[self.eofTerminal.name] = self.acceptedTerminal.name
                        finalState = itemSet

                    else:
                        # if it is not the final state, register REDUCE action using followDict
                        if debug:
                            print(' ^ registering reduce action')
                            print(item.grammar.left)
                        # print(self.followDict[item.grammar.left])
                        for symbol in self.getFollowSymbolOf(item.grammar.left):
                            itemSet.next[symbol] = item.grammarId

                    continue

                # implicit else

                # current symbol is the symbol just before the dot
                curentSymbol = item.getNext()

                # move the dot of that item
                newItem = Item(item.grammar,item.grammarId,item.next+1)


                # check if this itemset already has link to that element you want to expand
                if curentSymbol in itemSet.next:
                    if debug:
                        print(' ^ itemSet already has link to this item')
                    # move the new item into that item set in the next entry
                    itemSet.next[curentSymbol].addItem(newItem, True)
                else:
                    if debug:
                        print(' ^ creating new itemset')
                    # create a new item set
                    currentitemSetId+=1
                    newItemSet = ItemSet(newItem,currentitemSetId)

                    # link the item set
                    itemSet.next[curentSymbol] = newItemSet
                    if debug:
                        print('============checking itemSet.next=====================')
                        print('itemSet.next len: '+str(len(itemSet.next)))
                        for key, value in itemSet.next.items():
                            value.print()
                # continue to the next item

            # fixing duplicates
            for key in itemSet.next.keys():
                # if the itemSet.next[key] contains integer which denotes reduce operation
                # or if it is a string dentoes ACCEPTED_SYMBOL
                if isinstance(itemSet.next[key],int) or isinstance(itemSet.next[key],str):
                    # skip to the next entry, because no way it is duplicated
                    continue

                # implicit else
                expand = True

                for entry in self.itemSetList:
                    # duplicates found
                    if itemSet.next[key].compareInitial(entry):
                        # move the link reference
                        if debug:
                            print('..fixing duplicate')
                        itemSet.next[key] = entry
                        expand = False
                        currentitemSetId-=1
                        break

                if expand:
                    if debug:
                        print('..expanding')
                    itemSet.next[key].expand(self.grammarList)
                    # register the new item set to the item set list
                    self.itemSetList.append(itemSet.next[key])

        if debug:
            print("end state in itemset:")
            finalState.print()
        # self.finalItemState[]

    def __calculateFollow(self):
        # give the end character (EOF_SYMBOL($)) to the end of the first grammar
        self.followDict[self.grammarList[0].left]=list(self.eofTerminal.name)

        # symbol_dependTo_symbol is a list contain flattened record of symbol dependecy
        # ex: C and B depends on S will be stored as
        # symbol_dependTo_symbol [i]= 'CS'
        # symbol_dependTo_symbol [i+1]= 'BS'
        symbol_dependTo_symbol = list()
        for grammar in self.grammarList:
            for i in range (0,len(grammar.right)):
                if isNonTerminal(grammar.right[i]):
                    dependOnAnother = False
                    symbolToDepend = None

                    # if the symbol is already in the followDict entry
                    if grammar.right[i] not in self.followDict:
                        self.followDict[grammar.right[i]] = list()

                    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                    # WARNING: possible duplicate result if the gramMar is not well formed
                    # !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                    # if the symbol is at the end of the grammar
                    if i>= len(grammar.right)-1:
                        # it depends on the symbol in the left
                        symbolToDepend = grammar.left
                        dependOnAnother = True

                    # if the following symbol is a non terminal
                    elif isNonTerminal(grammar.right[i+1]):
                        # it depends on the next symbol
                        symbolToDepend = grammar.right[i+1]
                        dependOnAnother = True

                    # if it depends on other symbol
                    # link the entry to that symbol to this
                    if dependOnAnother:
                        # prevent duplicate in depedency entry
                        if str(grammar.right[i]+'-'+symbolToDepend) in symbol_dependTo_symbol:
                            continue

                        symbol_dependTo_symbol.append(grammar.right[i]+'-'+symbolToDepend)
                        if symbolToDepend not in self.followDict:
                            self.followDict[symbolToDepend] = list()
                        followingSymbol = self.followDict[symbolToDepend]

                    else:
                        # if not, put the next terminal symbol as the followingSymbol
                        followingSymbol = grammar.right[i+1]

                    # register whatever symbol or link to the followDict entry
                    self.followDict[grammar.right[i]].append(followingSymbol)

        # print the followDict
    def __recursivePrintFollowDict__(self,dictEntry):
        for symbol in dictEntry:
            # recursive call for entry that depends on other entry
            if isinstance(symbol,list):
                self.__recursivePrintFollowDict__(symbol)
            else:
                print(symbol+',', end='')

    def printFollowDict(self):
        for key, value in self.followDict.items():
            print(key+" : {", end='')
            self.__recursivePrintFollowDict__(value)
            print('}')


    def __recursiveGetFollowSymbolOf__(self,symbolList):
        listOfSymbols = list()
        for symbol in symbolList:
            if isinstance(symbol,list):
                listOfSymbols+=self.__recursiveGetFollowSymbolOf__(symbol)
            else:
                listOfSymbols.append(symbol)
        return listOfSymbols

    def getFollowSymbolOf(self, nonTerminal):
        listOfSymbols = list()

        for symbol in self.followDict[nonTerminal]:
            if isinstance(symbol,list):
                listOfSymbols+=self.__recursiveGetFollowSymbolOf__(symbol)
            else:
                listOfSymbols.append(symbol)
        return listOfSymbols


    # def generateParsingTable(self):


        # parsing table is encoded in list[n][3]
        # where list[n] = [current symbol,next symbol, action, target]
        # example: currently with S, next symbol is id, the action is shift to 6
        # will be: list[0] = [S,id,SHIFT,6]
        # available actions: SHIFT, REDUCE, GOTO
        # self.parsingTable = list()
        #
        # for idx, itemSet in enumerate(self.itemSetList):
        #     if itemSet.next:
        #         print ('i: '+str(idx))
        #         print('------------')
        #     for key, value in itemSet.next.items():
        #         if isNonTerminal(key):
        #             print('  '+key+': Go To '+str(value.id))
        #         else:
        #             if value<0:
        #                 print('  '+key+': Reduce '+str(value.id))
        #             else:
        #                 print('  '+key+': Shift '+str(value.id))
        #
        #
        # self.parsingTable.append(list())
        # tableIdx = 0
        # for itemSet in self.itemSetList:
        #     for key, value in itemSet.next.items():
        #         if isNonTerminal(key):
        #             self.parsingTable[tableIdx].append([key,'GOTO',value])
        #
        #

        # add follow rules into each itemSet.next



    # inner class for action
    # class Action:
    #     # command list:
    #     # SHIFT
    #     # REDUCE
    #     def __init__(self, command:str,target:int):
    #         self.command = command
    #         self.target = target

        # def execute(self):
            # if self.ac

    # visualise the parsing table
    def printParsingTable(self):
        for idx, itemSet in enumerate(self.itemSetList):
            if itemSet.next:
                print ('i: '+str(idx))
                print('------------')
            for key, value in itemSet.next.items():
                if isinstance(value,int):
                    print('  '+key+': Reduce '+str(value))
                elif isinstance(value,str):
                    print('  '+key+': '+value)
                elif isNonTerminal(key):
                    print('  '+key+': Go To '+str(value.id))
                else:
                    print('  '+key+': Shift '+str(value.id))

    def parse(self, tokens, debug:bool = False):

        # edges for the syntax tree
        # edges = list()
        nodes = list()
        # edgePtr = -1
        nodePtr = -1

        # instantiate inital input into the syntax tree
        for token in tokens:
            # edges.append(Edge(None, None, token.name,0))
            nodes.append(Node(token,self.terminalTranslation, parent = None,children = None))

        tokens.append(self.eofTerminal)
        tokensLength = len(tokens)
        ptr = 0
        stateStack = Stack()
        symbolStack = Stack()
        stateStack.push(0)

        numberOfNum  = 0
        if debug:
            print("tokens list:")
            for token in tokens:
                if token.name == "num":
                    numberOfNum+=1
                print(token.name, end=' ')
            print('')

        for token in tokens:
            if token.name == "num":
                    numberOfNum+=1

        # goto = False
        # print(stateStack.peek())
        # print(self.itemSetList[stateStack.peek()].next)
        # for key, value in self.itemSetList[stateStack.peek()].next.items():
            # print(key+": "+str(value.id))
        # print(self.itemSetList[stateStack.peek()].)
        # print (self.itemSetList[stateStack.peek()].next)
        action = self.itemSetList[stateStack.peek()].next[tokens[ptr].name]

        if debug:
            print('input length: '+str(tokensLength))

        accepted = True
        try:
            # if strInput[inputPtr] == INPUT_DELIMITER:
                # inputPtr+=1
                # continue
            while ptr<=tokensLength and action != self.acceptedTerminal.name:
                if debug:
                    print("stateStack: %d, symbolStack: %s, inputPtr: %s"%(stateStack.peek(), symbolStack.peek(), tokens[ptr].name))
                if ptr == tokensLength:
                    print('ERROR: cannot reach ACCEPTED_SYMBOL')
                    accepted = False

                if isinstance(action,int):
                    # reduce
                    if debug:
                        print('Reduce using grammar: #%d: %s'%(action, self.grammarList[action].toString()))
                        # validate the symbol stack has sufficient symbol to be reduced
                    i = len(symbolStack.items)-1
                    if debug:
                        print('early i:'+str(i))
                    j = len(self.grammarList[action].right)-1
                    for j in range (j,-1,-1):
                        if debug:
                            print("j: "+str(j)+" "+self.grammarList[action].right[j])
                            print("i: "+str(i)+" "+symbolStack.items[i])

                        if self.grammarList[action].right[j] != symbolStack.items[i]:
                            print("ERROR: insufficient symbol table")
                            print('Last symbol stack:')
                            for symbol in symbolStack.items:
                                print(symbol, end=' ')
                            accepted = False
                            exit()

                        i -=1

                    # pop symbols from symbol stack
                    for symbol in self.grammarList[action].right:
                        symbolStack.pop()
                        stateStack.pop()

                    # assign new symbol from the grammar
                    symbolStack.push(self.grammarList[action].left)

                    # reduce operation on the syntax tree
                    # push a new parent using left side of the grammar
                    if debug:
                        # print("current edge: "+edges[edgePtr].symbol+" height: "+str(edges[edgePtr].height))
                        print("current node: "+nodes[nodePtr].symbol)

                    # if the current noe has no parent, set it to the newParent
                    # if edges[edgePtr].parent == None:
                    if nodes[nodePtr].parent == None:
                        # tmpEdge = edges[edgePtr]
                        tmpNode = nodes[nodePtr]
                    else:
                        tmpNode = nodes[nodePtr].parent
                        # tmpEdge = edges[edgePtr].parent
                        # while tmpNode.parent !=None:
                        while tmpNode.parent !=None:
                            # tmpEdge = tmpEdge.parent
                            tmpNode = tmpNode.parent
                        # if the current edge has already a parent, keep going to the parents untill you find an empty one
                        # and set the new parent onto it

                    # set the current edge's parent
                    # newParent = Edge(None,edges[edgePtr], self.grammarList[action].left, tmpEdge.height+1)
                    newParent = Node(self.grammarList[action].left,self.translationList[action],parent = None, children = None)

                    if debug:
                        print("new parent: "+newParent.symbol)
                        # print("new parent: "+newParent.symbol +" height: "+str(newParent.height))
                        # print('child will be: '+tmpEdge.symbol +" height: "+str(tmpEdge.height))

                    # tmpEdge.parent = newParent

                    # tmpGrammar = list()
                    # for symbol in self.grammarList[action].right:
                    #     if symbol == edges[edgePtr].symbol:
                    #         continue
                    #     tmpGrammar.append(symbol)
                    #
                    # if debug:
                    #     print('the grammar: ')
                    #     print (self.grammarList[action].toString())
                    #     print("tmp grammar: ")
                    #     for symbol in tmpGrammar:
                    #         print(symbol, end='')
                    #     print('')

                    reducedSymbols= 0
                    # !!!!!!!!!!!BRUTALITY FOR QUICK FIX
                    if action == 5 and numberOfNum >= 2:
                        # print("going to skip")
                        # print(self.grammarList[action].toString())
                        skipThis = True
                    else:
                        skipThis = False

                    for symbol in reversed(self.grammarList[action].right):
                        # if symbol == nodes[nodePtr].symbol:
                            # skip current edge
                            # if debug:
                                # print('skip current edge')
                            # continue

                        if debug:
                            print("reducedSymbols: "+str(reducedSymbols))

                            # continue
                        # for edge in reversed(edges):
                        for i in range (len(nodes)-1,-1,-1):
                        # for node in reversed(nodes):
                            if nodes[i].symbol == symbol and nodes[i].parent == None:
                            # if edge.symbol == symbol and edge.parent == None:
                                # found an edge, connect it to the new newParent
                                if debug:
                                    print('found node: '+nodes[i].symbol)
                                    # print('found edge: '+edge.symbol)
                                # edge.parent = newParent
                                if skipThis:
                                    # print('skip')
                                    skipThis = False
                                    i+=1
                                    continue
                                nodes[i].parent = newParent
                                newParent.children.append(nodes[i])
                                reducedSymbols+=1
                                break

                    if reducedSymbols<len(self.grammarList[action].right):
                        if debug:
                            print("reducedSymbols: "+str(reducedSymbols))
                        print('ERROR: failed to construct syntax tree')
                        exit()

                    # explisit else
                    # register the edge to edges
                    # edges.append(newParent)
                    nodes.append(newParent)

                    # check the symbol stack to decide goto action
                    action = self.itemSetList[stateStack.peek()].next[symbolStack.peek()]
                    # goto
                    if debug:
                        print('Go to state: '+str( action.id))

                    stateStack.push(action.id)
                    action = self.itemSetList[stateStack.peek()].next[tokens[ptr].name]
                    # elif isNonTerminal(action.next):

                else:
                    # shift
                    if debug:
                        print('Shift to state: '+str(action.id))
                    stateStack.push(action.id)
                    symbolStack.push(tokens[ptr].name)

                    ptr+=1
                    # edgePtr+=1
                    nodePtr+=1
                    action = self.itemSetList[stateStack.peek()].next[tokens[ptr].name]

        except KeyError:
            print("ERROR: illegal syntax")
            # print("last state:")
            # print("stateStack: %d, symbolStack: %s, inputPtr: %s"%(stateStack.peek(), symbolStack.peek(), tokens[ptr].name))
            accepted = False

        # if accepted:
            # print("Accepted!")

        # returning syntax tree
        # self.syntaxTree = edges
        self.syntaxTree = nodes
        return nodes

    def __recursivePrintSyntaxTree(self, edge:Edge, indent):
        if edge == None:
            return

        print(indent,end='')
        if isinstance(edge.token,Token):
            print(edge.symbol+': '+str(edge.token.value))
        else:
            print(edge.symbol)

        if edge.children:
            indent+='-'
            for child in reversed(edge.children):
                self.__recursivePrintSyntaxTree(child, indent)

    # printing the syntax tree in depth-first search way
    def printSyntaxTree(self):
        if self.syntaxTree == None:
            print("no syntax tree, please parse something")
            return
        indent =  ''
        self.__recursivePrintSyntaxTree(self.syntaxTree[-1], indent)

    def compile(self):
        if not self.syntaxTree:
            print("please parse sth first")
            return None

        self.syntaxTree[-1].trans()



class Stack:
    def __init__(self):
        self.items = []

    def isEmpty(self):
        return self.items == []

    def isNotEmpty(self):
        return self.items != []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def peek(self):
        if self.isEmpty():
            return '<NONE>'
        return self.items[len(self.items)-1]

    def size(self):
        return len(self.items)

class ItemSet:
    def __init__(self, kernel:Item, id:int):

        # items contains list of Items object
        self.items = list()
        self.initialItems = list()
        self.items.append(kernel)
        self.initialItems.append(kernel)
        self.next = dict()
        self.listOfExpanded = list()

        self.id = id

    def addItem(self,item:Item, inital:bool=False):
        self.items.append(item)
        if inital:
            self.initialItems.append(item)


    # compare what are inside the itemset with otherItemSet
    # return true if they are the same
    # return false if they are not
    def compare(self,otherItemSet):
        for i in range (0, len(self.items)):
            if self.items[i].toString() != (otherItemSet.items[i].toString()):
                return False
        return True

    def compareInitial(self,otherItemSet):
        for i in range (0, len(self.items)):
            if self.initialItems[i].toString() != (otherItemSet.initialItems[i].toString()):
                return False
        return True

    def expand(self, grammarList:list):
        debug = False
        # if (self.id == 4):
            # debug = True
            # print('expanding i: '+str(self.id))
        #  looping through the items
        for item in self.items:
            if debug:
                print("item: "+item.toString())
            #  if  -> A -> S . (end of a grammar)
            if item.next >= len(item.grammar.right):
                if debug:
                    print('complete')
                continue

            # if the the next character after the dot is a non terminal:
            nextCharacter = item.getNext()
            if debug:
                print('next Character: '+nextCharacter)
            if isNonTerminal(nextCharacter) and nextCharacter not in self.listOfExpanded:
                if debug:
                    print("expanding "+nextCharacter)

                # take every character in the grammar list which has the next character
                # how to optimize?
                for idx, grammar in enumerate(grammarList):
                    if (grammar.left == nextCharacter):
                        if debug:
                            print("get grammar: "+grammar.toString())
                        newItem = Item(grammar, idx, 0)
                        self.addItem(newItem)

                # register the non terminal into listOfExpanded to prevent double
                self.listOfExpanded.append(nextCharacter)

            # continue

    def print(self):
        print('================')
        print('---i: '+str(self.id)+'---')
        for item in self.items:
            print(item.toString())
        print('================')
