# print((int('1')))

import re
import sys
from classes import Grammar
from classes import Terminal
from classes import Item
from classes import ItemSet
from classes import ParsingTable
import lexical_analysis as lexer
import linked_list

grammarList = [
        Grammar('S',['A','name']),
        Grammar('S',['B','at_idx','num']),
        Grammar('S',['D','at_idx','num']),
        Grammar('S',['B','last']),
        Grammar('S',['D','last']),
        Grammar('B',['B_','num']),
        Grammar('B_',['B__','name']),
        Grammar('B__',['add']),
        Grammar('B__',['update']),
        Grammar('D',['D_','name']),
        Grammar('D_',['clear']),
        Grammar('D_',['print']),
        # Grammar('C',['at_idx','num']),
        # Grammar('C',['at_idx','last']),
        Grammar('A',['create'])
        ]

translationList = [
    linked_list.exec_s,
    linked_list.exec_s,
    linked_list.exec_s,
    linked_list.exec_s,
    linked_list.exec_s,
    linked_list.exec_b,
    linked_list.exec_b_,
    linked_list.getValue,
    linked_list.getValue,
    linked_list.exec_d,
    linked_list.getValue,
    linked_list.getValue,
    linked_list.getValue
]

terminalTranslation = linked_list.getValue

# create linkedlinst-> getVal, exec_s(command, value)

# grammarList = [
#         Grammar('E',['E','+','T']),
#         Grammar('E',['T']),
#         Grammar('T',['T','*','F']),
#         Grammar('T',['F']),
#         Grammar('F',['(','E',')']),
#         Grammar('F',['id']),
#         ]

# grammarList = [
#         Grammar('S',['(','L',')']),
#         Grammar('S',['x']),
#         Grammar('L',['S']),
#         Grammar('L',['L',',','S'])
#         ]



ACCEPTED_TERMINAL = Terminal('ACC',['ACC'],False)
EOF_TERMINAL = Terminal('$',['$'],False)

terminalList = {
        'name': Terminal('name',[],False),
        'num': Terminal('num',[],False),
        'add': Terminal('add',['add'],False),
        'update': Terminal('update',['update'],False),
        'create': Terminal('create',['create'],False),
        'clear': Terminal('clear',['clear'],False),
        'print': Terminal('print',['print'],False),
        'at_idx': Terminal('at_idx',['at_idx'],False),
        'last': Terminal('last',['last'],False),
        # 'null': Terminal('null',['null'],False),
        # 'last': Terminal('last',['last'],False),
        '$': EOF_TERMINAL,
        'ACC': ACCEPTED_TERMINAL
    }

# terminalList = {
#         'id': Terminal('id',['id'],False),
#         '+': Terminal('+',['+'],False),
#         '*': Terminal('*',['*'],False),
#         '(': Terminal('(',['('],False),
#         ')': Terminal(')',[')'],False),
#         '$': EOF_TERMINAL,
#         'ACC': ACCEPTED_TERMINAL
#     }

# terminalList = {
#         'x': Terminal('x',['x'],False),
#         ',': Terminal(',',[','],False),
#         '(': Terminal('(',['('],False),
#         ')': Terminal(')',[')'],False),
#         '$': EOF_TERMINAL,
#         'ACC': ACCEPTED_TERMINAL
#     }

lex = lexer.Lexer(grammarList, terminalList)
parsingTable = ParsingTable(grammarList, translationList, terminalTranslation,EOF_TERMINAL, ACCEPTED_TERMINAL)

while True:
    # print('> ',end='')
    user_input= input('> ')
    lexemes = lex.tokenize(user_input)
    parsingTable.parse(lexemes)
    parsingTable.compile()
# lexemes = lex.tokenize('id * ( id + id ) + ( id * id ) + id')
# lexemes = lex.tokenize('( x , ( x ) )')
# lexemes = lex.tokenize('1 * 2 + 3')
# lexemes0 = lex.tokenize('create nama2')
# lexemes1 = lex.tokenize('add nama2 10 last')
# lexemes2 = lex.tokenize('add nama2 11 last')
# lexemes3 = lex.tokenize('add nama2 12 last')
# lexemes4 = lex.tokenize('add nama2 13 last')
# # lexemes5 = lex.tokenize('clear nama2 last')
# # lexemes6 = lex.tokenize('clear nama2 at_idx 1')
# lexemes5 = lex.tokenize('print nama2 last')
# lexemes6 = lex.tokenize('print nama2 at_idx 2')
#
#
#
# # lexemes2 = lex.tokenize('add nama 14 at_idx 4')
#
# # lexemes = lex.tokenize('print nama last')
#
#
# # lexemes = lex.tokenize('create nama')
#
#
#
# # lex.printSymbolTable()
# # parsingTable.print()
# # parsingTable.printParsingTable()
# # parsingTable.printFollowDict()
#
# parsingTable.parse(lexemes0)
# parsingTable.compile()
# parsingTable.parse(lexemes1)
#
# # for item in lexemes1:
#     # if isinstance(item,Terminal):
#         # print(item.name+'  '+str(item))
#     # else:
#         # print(item.name+'  '+str(item.value))
# #
# # parsingTable.printSyntaxTree()
# # lex.printSymbolTable()
# parsingTable.compile()
# parsingTable.parse(lexemes2)
# parsingTable.compile()
# parsingTable.parse(lexemes3)
# parsingTable.compile()
# parsingTable.parse(lexemes4)
# parsingTable.compile()
# parsingTable.parse(lexemes5)
# parsingTable.compile()
# parsingTable.parse(lexemes6)
# parsingTable.compile()
# parsingTable.closure()
# print('\n\n')
# parsingTable.print()
# parsingTable.generateParsingTable()
# print('\n\n')
# parsingTable.calculateFollow()
# parsingTable.printFollowDict()
# parsingTable.printParsingTable()
# parsingTable.parse(lexer.tokenize('create newLinkedList'), True)
# regex for linked list naming
# nameRegex = re.compile('[A-z]')
# STILL BROKEN regex for linkedlist's element naming
# elementRegex = re.compile('\d')
# lex.printSymbolTable()
# print(nameRegex)
# print(nameRegex.match('AAA'))
# print(nameRegex.match('12aa'))
# print(nameRegex.match('aa aa123'))
# print(nameRegex.match('zza12z'))

# print(elementRegex.match('123'))
# print(elementRegex.match('123'))
# print(elementRegex.match('1aa2aa3'))
# print(elementRegex.match('aa123'))

# print(isinstance(nameRegex,re))
#
# terminalList = [
#         Terminal('a',['create'],False),
#         Terminal('b',['add','update'],False),
#         Terminal('name',nameRegex,True),
#         Terminal('element',nameRegex,True)
#         ]
#

        # b_.addRight(['add','name'])
# b_.addRight(['update','name'])

# print(isinstance(b_,Grammar))
